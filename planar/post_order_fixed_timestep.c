// 32  1.51e-05 0.000167 0.000167 0.000167 5.07e-06 3.6e-05 1e-06
// 64  5.52e-06 8.78e-05 8.78e-05 8.78e-05 2.93e-06 4.14e-05 1e-06
// 128 1.4e-06  2.28e-05 2.28e-05 2.28e-05 1.08e-06 1.78e-05 1e-06
// 256 3.32e-07 4.73e-06 4.73e-06 4.73e-06 3.01e-07 4.37e-06 1e-06


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void main(){

// TODO: should write something that reads the log file.
	double tab[4]= {32,64,128,256};
	double error[4] = {1.51e-05,5.52e-06,1.4e-06 ,3.32e-07}; // avg all
	double error2[4] = {0.000167,8.78e-05,2.28e-05,4.73e-06}; // max all
	double error3[4] = {0.000167,8.78e-05,2.28e-05,4.73e-06}; // avg partial

	double error4[4] = {0.000167,8.78e-05,2.28e-05,4.73e-06}; // max partial
	double error5[4] = {5.07e-06,2.93e-06,1.08e-06,3.01e-07}; // avg full
	double error6[4] = {3.6e-05,4.14e-05,1.78e-05,4.37e-06}; // max full
	double order[3],order2[3],order3[3],order4[3],order5[3],order6[3];

   FILE *fp;
   fp = fopen("output_order_fixed", "w"); // read mode
	fprintf(fp,"avg_all, max_all avg_partial, max_partial avg_full, max_full\n");
	for (int i = 1; i <= 3; i++){
		// order[i] = log(tab[i]-tab[i-1]);
		order[i] = -(log(error[i])-log(error[i-1]))/(log(tab[i])-log(tab[i-1]));
		order2[i] = -(log(error2[i])-log(error2[i-1]))/(log(tab[i])-log(tab[i-1]));
		order3[i] = -(log(error3[i])-log(error3[i-1]))/(log(tab[i])-log(tab[i-1]));
		order4[i] = -(log(error4[i])-log(error4[i-1]))/(log(tab[i])-log(tab[i-1]));
		order5[i] = -(log(error5[i])-log(error5[i-1]))/(log(tab[i])-log(tab[i-1]));
		order6[i] = -(log(error6[i])-log(error6[i-1]))/(log(tab[i])-log(tab[i-1]));
		fprintf(fp,"%.3g %.3g %.3g %.3g %.3g %.3g\n",
			order[i],order2[i],order3[i],order4[i],order5[i],order6[i]);
	}
	fclose(fp);
}



