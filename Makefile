DOCNAME=main3

all: main3

.PHONY: clean

main3:
	pdflatex -quiet -c-style-errors -halt-on-error -synctex=1 $(DOCNAME).tex
	bibtex $(DOCNAME).aux
	pdflatex -quiet -c-style-errors -interaction=nonstopmode -synctex=1 $(DOCNAME).tex
	pdflatex -quiet -c-style-errors -interaction=nonstopmode -synctex=1 $(DOCNAME).tex

view: main3
	evince $(DOCNAME).pdf &

clean:
	rm *.blg *.bbl *.aux *.log