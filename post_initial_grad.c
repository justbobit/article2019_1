#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// 32   0.000159 0.000531 3.48e-05 3.48e-05 0.000164 0.000531 0.000838719
// 64   6.52e-05 0.000252 5.19e-05 5.19e-05 6.53e-05 0.000252 0.0004
// 128  1.55e-05 6.46e-05 7.16e-06 7.16e-06 1.56e-05 6.46e-05 0.0001
// 256  4.06e-06 1.63e-05 6.92e-06 6.92e-06 4.04e-06 1.63e-05 2.5e-05


void main(){

// TODO: should write something that reads the log file.
  double tab[4]= {32,64,128,256};
  double error[4] = {0.000318001,8.04334e-05,2.02263e-05,5.07139e-06}; // avg all
  double order[3];

   FILE *fp;
   fp = fopen("output_order", "w"); // read mode
  fprintf(fp,"error\n");
  fprintf(fp, "%5.3g\n", error[0]);
  for (int i = 1; i <= 3; i++){
    // order[i] = log(tab[i]-tab[i-1]);
    order[i] = -(log(error[i])-log(error[i-1]))/(log(tab[i])-log(tab[i-1]));
    fprintf(fp," %5.3g %5.3g\n",error[i],order[i]);
  }
  fclose(fp);
}



