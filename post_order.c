#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// 32   0.000159 0.000531 3.48e-05 3.48e-05 0.000164 0.000531 0.000838719
// 64   6.52e-05 0.000252 5.19e-05 5.19e-05 6.53e-05 0.000252 0.0004
// 128  1.55e-05 6.46e-05 7.16e-06 7.16e-06 1.56e-05 6.46e-05 0.0001
// 256  4.06e-06 1.63e-05 6.92e-06 6.92e-06 4.04e-06 1.63e-05 2.5e-05


void main(){

// TODO: should write something that reads the log file.
	double tab[4]= {32,64,128,256};
	double error[4] = {1.59e-4,6.52e-5,1.55e-5,4.06e-6}; // avg all
	double error2[4] = {0.000531,0.000252,6.46e-05,1.63e-05}; // max all
	double error3[4] = {3.48e-05,5.19e-05,7.16e-06,6.92e-06}; // avg partial
	double error4[4] = {3.48e-05,5.19e-05,7.16e-06,6.92e-06}; // max partial
	double error5[4] = {0.000164,6.53e-05,1.56e-05,4.04e-06 }; // avg full
	double error6[4] = {0.000531,0.000252,6.46e-05,1.63e-05 }; // max full
	double order[3],order2[3],order3[3],order4[3],order5[3],order6[3];

   FILE *fp;
   fp = fopen("output_order", "w"); // read mode
	fprintf(fp,"avg_all, max_all avg_partial, max_partial avg_full, max_full\n");
	for (int i = 1; i <= 3; i++){
		// order[i] = log(tab[i]-tab[i-1]);
		order[i] = -(log(error[i])-log(error[i-1]))/(log(tab[i])-log(tab[i-1]));
		order2[i] = -(log(error2[i])-log(error2[i-1]))/(log(tab[i])-log(tab[i-1]));
		order3[i] = -(log(error3[i])-log(error3[i-1]))/(log(tab[i])-log(tab[i-1]));
		order4[i] = -(log(error4[i])-log(error4[i-1]))/(log(tab[i])-log(tab[i-1]));
		order5[i] = -(log(error5[i])-log(error5[i-1]))/(log(tab[i])-log(tab[i-1]));
		order6[i] = -(log(error6[i])-log(error6[i-1]))/(log(tab[i])-log(tab[i-1]));
		fprintf(fp,"%g %g %g %g %g %g\n",
			order[i],order2[i],order3[i],order4[i],order5[i],order6[i]);
	}
	fclose(fp);
}



