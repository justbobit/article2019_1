#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// 32  1.971e-03 4.863e-03 4.863e-03 4.863e-03 1.904e-03 4.564e-03 7.944423e-03
// 64  3.800e-04 6.398e-04 3.107e-04 3.108e-04 3.811e-04 6.398e-04 2.500000e-03
// 128 8.306e-05 1.419e-04 1.419e-04 1.419e-04 8.272e-05 1.359e-04 6.250000e-04
// 256 2.002e-05 6.060e-05 6.060e-05 6.060e-05 1.999e-05 5.811e-05 1.562500e-04



void main(){

// TODO: should write something that reads the log file.
	double tab[4]= {32,64,128,256};
	double error[4] = {1.971e-03,3.800e-04,8.306e-05,2.002e-05}; // avg all
	double error2[4] = {4.863e-03,6.398e-04,1.419e-04,6.060e-05}; // max all
	double error3[4] = {4.863e-03,6.398e-04,1.419e-04,6.060e-05}; // avg all
	double error4[4] = {3.48e-05,5.19e-05,7.16e-06,6.92e-06}; // max partial
	double error5[4] = {0.000164,6.53e-05,1.56e-05,4.04e-06 }; // avg full
	double error6[4] = {0.000531,0.000252,6.46e-05,1.63e-05 }; // max full
	double order[3],order2[3],order3[3],order4[3],order5[3],order6[3];

   FILE *fp;
   fp = fopen("output_order", "w"); // read mode
	fprintf(fp,"avg_all, max_all avg_partial, max_partial avg_full, max_full\n");
	for (int i = 1; i <= 3; i++){
		// order[i] = log(tab[i]-tab[i-1]);
		order[i] = -(log(error[i])-log(error[i-1]))/(log(tab[i])-log(tab[i-1]));
		order2[i] = -(log(error2[i])-log(error2[i-1]))/(log(tab[i])-log(tab[i-1]));
		order3[i] = -(log(error3[i])-log(error3[i-1]))/(log(tab[i])-log(tab[i-1]));
		order4[i] = -(log(error4[i])-log(error4[i-1]))/(log(tab[i])-log(tab[i-1]));
		order5[i] = -(log(error5[i])-log(error5[i-1]))/(log(tab[i])-log(tab[i-1]));
		order6[i] = -(log(error6[i])-log(error6[i-1]))/(log(tab[i])-log(tab[i-1]));
		fprintf(fp,"%g %g %g %g %g %g\n",
			order[i],order2[i],order3[i],order4[i],order5[i],order6[i]);
	}
	fclose(fp);
}



