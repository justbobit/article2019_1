#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// 32  3.672e-04 5.649e-04 8.420e-05 8.427e-05 3.714e-04 5.649e-04 1.000000e-03
// 64  6.450e-05 1.673e-04 4.401e-05 4.405e-05 6.482e-05 1.673e-04 2.500000e-04
// 128 2.273e-05 5.923e-05 5.801e-05 5.802e-05 2.253e-05 5.923e-05 6.250000e-05
// 256 4.383e-06 2.386e-05 2.385e-05 2.386e-05 4.367e-06 2.294e-05 1.562500e-05


void main(){

// TODO: should write something that reads the log file.
  double tab[4]= {32,64,128,256};
  double error[4] = { 3.672e-04,6.450e-05,2.273e-05,4.383e-06}; // avg all
  double error2[4] = {5.649e-04,1.673e-04,5.923e-05,2.386e-05}; // max all
  double error3[4] = {8.420e-05,4.401e-05,5.801e-05,2.385e-05}; // avg partial
  double error4[4] = {8.427e-05,4.405e-05,5.802e-05,2.386e-05}; // max partial
  double error5[4] = {3.714e-04,6.482e-05,2.253e-05,4.367e-06}; // avg full
  double error6[4] = {5.649e-04,1.673e-04,5.923e-05,2.294e-05}; // max full
  double order[3],order2[3],order3[3],order4[3],order5[3],order6[3];

   FILE *fp;
   fp = fopen("output_order_uday", "w"); // read mode
  fprintf(fp,"avg_all, max_all avg_partial, max_partial avg_full, max_full\n");
  for (int i = 1; i <= 3; i++){
    // order[i] = log(tab[i]-tab[i-1]);
    order[i] = -(log(error[i])-log(error[i-1]))/(log(tab[i])-log(tab[i-1]));
    order2[i] = -(log(error2[i])-log(error2[i-1]))/(log(tab[i])-log(tab[i-1]));
    order3[i] = -(log(error3[i])-log(error3[i-1]))/(log(tab[i])-log(tab[i-1]));
    order4[i] = -(log(error4[i])-log(error4[i-1]))/(log(tab[i])-log(tab[i-1]));
    order5[i] = -(log(error5[i])-log(error5[i-1]))/(log(tab[i])-log(tab[i-1]));
    order6[i] = -(log(error6[i])-log(error6[i-1]))/(log(tab[i])-log(tab[i-1]));
    fprintf(fp,"%g %g %g %g %g %g\n",
      order[i],order2[i],order3[i],order4[i],order5[i],order6[i]);
  }
  fclose(fp);
}



